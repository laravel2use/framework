<?php

namespace Laravel2Use\Extension\Addons\Commands;

use Jumilla\Addomnipot\Laravel\Commands\AddonStatusCommand as BaseCommand;

class AddonStatusCommand extends BaseCommand
{
    /**
     * Create a new console command instance.
     */
    public function __construct()
    {
        $this->description = '[+] '.$this->description;

        parent::__construct();
    }
}
