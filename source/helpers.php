<?php

use Laravel2Use\Extension\Specs\Factory as SpecFactory;

if (!function_exists('spec')) {
    /**
     * Get spec.
     *
     * @param string $path
     *
     * @return \Laravel2Use\Extension\Specs\Factory|Laravel2Use\Extension\Specs\InputSpec
     */
    function spec($path = null)
    {
        $factory = app(SpecFactory::class);

        if (func_num_args() == 0) {
            return $factory;
        }

        return $factory->make($path);
    }
}
